import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    //variable for checking current user
    static public int currentUserId = 0;
    static String topic;
    static char again = 'y';


    //DataBase Class
    static DataBase dataBase = new DataBase();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        char nextTry = 'y';
        while (currentUserId == 0 && nextTry =='y') {
            System.out.println("What you want to do?");
            System.out.println("l - login");
            System.out.println("c - create an account");
            char action = scanner.nextLine().charAt(0);

            if (action == 'l') {
                login();
            } else if (action == 'c') {
                createUser();
            }
            if (currentUserId > 0) {
                while (again == 'y') {
                    System.out.println("What you want to do?");
                    System.out.println("t - test");
                    System.out.println("r - read data");
                    char choice = scanner.nextLine().charAt(0);

                    if (choice == 't') {
                        takeTest();
                    } else if (choice == 'r') {
                        readData();
                    }
                    System.out.println("Do you want to do something more? y/n");
                    again = scanner.nextLine().charAt(0);
                }
            } else {
                System.out.println("Incorrect login or no account!");
                System.out.println("Do you want to try again? y/n");
                nextTry = scanner.nextLine().charAt(0);
            }
        }

    }

    public static void login() {
        UserInfo currentUser = new UserInfo();

            System.out.println("Enter username");
            currentUser.setUsername(scanner.nextLine());

            System.out.println("Enter password");
            currentUser.setPswd(scanner.nextLine());

            //DataBase class method for login check returns current Users ID Nr.
            int userId = dataBase.checkLogin(currentUser.getUsername(), currentUser.getPswd());
        if (userId>0) {
            System.out.println("You have logged in successfully!");
            currentUserId = userId;
        }
    }

    public static void createUser(){
        UserInfo newUser = new UserInfo();
        System.out.println("Enter username");
        newUser.setUsername(scanner.nextLine());

        //to create a method that we can use for others as well
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{3,20}$");
        Matcher matcher = pattern.matcher(newUser.getUsername());
        while (matcher.matches()== false) {
            System.out.println("Please enter a valid username! It should be at least 3 characters!");
            newUser.setUsername(scanner.nextLine());
            pattern = Pattern.compile("^[a-zA-Z0-9]{3,20}$");
            matcher = pattern.matcher(newUser.getUsername());
        }

        //DataBase class method for username check returns 1 if true, 0 if false
        int userId = dataBase.checkUserName(newUser.getUsername());

        while (userId > 0 ) {
            System.out.println("Username already exists! Try another one!");
            newUser.setUsername(scanner.nextLine());
            pattern = Pattern.compile("^[a-zA-Z0-9+_.!-]]{3,20}$");
            matcher = pattern.matcher(newUser.getUsername());
            while (matcher.matches() == false) {

                System.out.println("Please enter a valid username! It should be at least 3 characters!");
                newUser.setUsername(scanner.nextLine());
                pattern = Pattern.compile("^[a-zA-Z0-9+_.!-]]{3,20}$");
                matcher = pattern.matcher(newUser.getUsername());
            }
            userId = dataBase.checkUserName(newUser.getUsername());

        }

        System.out.println("Enter password");
        newUser.setPswd(scanner.nextLine());

        pattern = Pattern.compile("^[a-zA-Z0-9+_.!-]{3,20}$");
        matcher = pattern.matcher(newUser.getPswd());
        while (matcher.matches()== false) {
            System.out.println("Please enter a valid password! It should be at least 3 characters!");
            newUser.setPswd(scanner.nextLine());
            pattern = Pattern.compile("^[a-zA-Z0-9+_.!-]{3,20}$");
            matcher = pattern.matcher(newUser.getPswd());
        }

        System.out.println("Enter full name");
        newUser.setFullName(scanner.nextLine());

        pattern = Pattern.compile("^([A-Za-z]*((\\s)))+[A-Za-z]*$");
        matcher = pattern.matcher(newUser.getFullName());
        while (matcher.matches()== false) {
            System.out.println("Please enter valid first name and last name!");
            newUser.setFullName(scanner.nextLine());
            pattern = Pattern.compile("^([A-Za-z]*((\\s)))+[A-Za-z]*$");
            matcher = pattern.matcher(newUser.getFullName());
        }

        System.out.println("Enter email");
        newUser.setEmail(scanner.nextLine());
        pattern = Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");
        matcher = pattern.matcher(newUser.getEmail());
        while (matcher.matches()== false) {
            System.out.println("Please enter valid e-mail!");
            System.out.println("Enter email");
            newUser.setEmail(scanner.nextLine());
            pattern = Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");
            matcher = pattern.matcher(newUser.getEmail());
        }

        //CurrentUserID returns new userID Nr.
        currentUserId = dataBase.createUser(newUser.getUsername(), newUser.getPswd(), newUser.getFullName(), newUser.getEmail());

        if (currentUserId>0){
            System.out.println("You have created an account successfully!");
        }
    }

    public static void takeTest(){
        System.out.println("Which test do you want to take?");
        System.out.println("1 - +- Plus/minus");
        System.out.println("2 - * Multiplication");
        char testChoice = scanner.nextLine().charAt(0);

        if (testChoice == '1') {
            topic = "plus_minus";
            if(dataBase.testIsDone(currentUserId,topic)>0){
                System.out.println("Test is already done");
            }else{
                //DataBase class method for reading questions and recording results
                dataBase.Questions(topic,currentUserId);}

        } else if (testChoice == '2') {
            topic = "multiplication";
            if(dataBase.testIsDone(currentUserId,topic)>0){
                System.out.println("Test is already done");
            }else{
                //DataBase class method for reading questions and recording results
                dataBase.Questions(topic,currentUserId);}
        }
    }

    public static void readData(){
        System.out.println("Which data do you want to read?");
        System.out.println("1 - my results");
        System.out.println("2 - all results");
        char readChoice = scanner.nextLine().charAt(0);
        if (readChoice == '1') {
            System.out.println("Please choose topic");
            System.out.println("1 - Plus/minus");
            System.out.println("2 - Multiplication");
            char chooseTopic = scanner.nextLine().charAt(0);
            if(chooseTopic == '1'){
                topic = "plus_minus";
                if (dataBase.testIsDone(currentUserId,topic)>0)
                    {dataBase.readMyResults(currentUserId, topic);
                } else {
                    System.out.println("Test is not done yet");
                }
            } else if (chooseTopic == '2'){
                topic = "multiplication";
                if (dataBase.testIsDone(currentUserId,topic)>0)
                    {dataBase.readMyResults(currentUserId, topic);
                } else {
                    System.out.println("Test is not done yet");
                }
            }
        } else if (readChoice == '2') {
            System.out.println("Please choose topic");
            System.out.println("1 - Plus/minus");
            System.out.println("2 - Multiplication");
            char chooseTopic = scanner.nextLine().charAt(0);
            if(chooseTopic == '1'){
                topic = "plus_minus";
                dataBase.readAllResults(topic);
                dataBase.readZeroResults(topic);
            } else if (chooseTopic == '2'){
                topic = "multiplication";
                dataBase.readAllResults(topic);
                dataBase.readZeroResults(topic);
            }
        }
    }
}

