import java.sql.*;
import java.util.Locale;
import java.util.Scanner;

public class DataBase {
    private final String dbURL = "jdbc:mysql://localhost:3306/java27";
    private final String user = "root";
    private final String password = ".";


    public int createUser(String userName, String pswd, String fullName, String email){
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            String sql = "INSERT INTO users (username, password, fullname, email) VALUES (?,?,?,?);";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, pswd);
            preparedStatement.setString(3, fullName);
            preparedStatement.setString(4, email);
            preparedStatement.executeUpdate();


            //to get the ID of the current user
            String sqlID = "SELECT * FROM users WHERE username ='" + userName + "' and password ='" + pswd + "'";
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlID);


            if (resultSet.next()) {
                return resultSet.getInt(1);//returns current users ID Nr.
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  0;
    }

    public int checkUserName(String userName){

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sqlUser = "SELECT * FROM users WHERE username ='" + userName + "'";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlUser);

            if (resultSet.next()) {

                //returns Users ID Nr.
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int checkLogin(String userName, String pswd){

        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "SELECT * FROM users WHERE username ='" + userName + "' and password ='" + pswd + "'";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                //returns current Users ID Nr.
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int testIsDone (int userID,String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "select COUNT(*) \n" +
                    "from quiz, testResults\n" +
                    "where testResults.questionID = quiz.questionID \n" +
                    "AND userID=" + userID + " \n" +
                    "AND topic = '" + topic + "' \n";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                return resultSet.getInt(1);//returns number of answered questions
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int Questions(String topic, int userID){
        // create select to get all questions
        // count points and return them
        //insert data in testResult table
        Scanner scanner = new Scanner(System.in);
        int points = 0;


        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "SELECT * FROM quiz WHERE topic ='" + topic + "'";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                String question = resultSet.getString(3);
                int answerPoints = resultSet.getInt(5);
                System.out.println(question);
                System.out.println("Points you can get: " + answerPoints );

                String providedAnswer = scanner.nextLine().toLowerCase(Locale.ROOT).trim();

                String sql2 = "INSERT INTO testResults (userID, questionID, answer) VALUES (?,?,?);";

                PreparedStatement preparedStatement2 = conn.prepareStatement(sql2);
                preparedStatement2.setInt(1, userID);
                preparedStatement2.setInt(2, resultSet.getInt(1));
                preparedStatement2.setString(3, providedAnswer);
                preparedStatement2.executeUpdate();

                if (providedAnswer.equals(resultSet.getString(4))){
                    System.out.println("Correct");
                    points = points + resultSet.getInt(5);

                } else {
                    System.out.println("Incorrect, correct answer is " + resultSet.getString(4));
                }

            }

            System.out.println("You've got " + points + " points");
            return points;


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void readMyResults(int userID, String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            String sql = "SELECT COUNT(*) \n" +
                    "FROM quiz, testResults\n" +
                    "WHERE testResults.questionID = quiz.questionID \n" +
                    "AND userID=" + userID + " \n" +
                    "AND topic = '" + topic + "' \n" +
                    "AND testResults.answer=quiz.answer;";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                int correctAnswerCount = resultSet.getInt(1);
                int countQuestions = questionCount(topic);
                int maxPoints = maxPoints(topic);
                int userPoints = userPoints(userID, topic);

                System.out.println(correctAnswerCount + " correct answers " +
                        "of " + countQuestions + ".\n You've got " + userPoints +
                        " points of " + maxPoints+".");

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int questionCount(String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "SELECT COUNT(*) \n" +
                    "FROM quiz\n" +
                    "WHERE topic = '" + topic + "';";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int maxPoints(String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "SELECT SUM(points) \n" +
                    "FROM quiz\n" +
                    "WHERE topic = '" + topic + "';";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int userPoints(int userID,String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {
            String sql = "SELECT SUM(points)\n" +
                    "FROM quiz, testResults\n" +
                    "WHERE testResults.questionID = quiz.questionID\n" +
                    "AND quiz.answer = testResults.answer\n"+
                    "AND userID=" + userID + " \n" +
                    "AND topic = '" + topic + "' \n";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void readAllResults(String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {


            String sql = "SELECT username, SUM(points)\n" +
                    "FROM quiz, testResults, users\n" +
                    "WHERE testResults.questionID = quiz.questionID\n" +
                    "AND topic = '"  + topic + "' \n" +
                    "AND users.user_id = testResults.userID\n" +
                    "AND quiz.answer = testResults.answer\n" +
                    "GROUP BY username\n"+
                    "ORDER BY SUM(points) DESC";

            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {


                String username = resultSet.getString(1);
                int points = resultSet.getInt(2);


                String output = "User info: \n\t username: %s \n\t total points: %d";

                System.out.println(String.format(output, username, points));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void readZeroResults (String topic) {
        try (Connection conn = DriverManager.getConnection(dbURL, user, password)) {

            String sql = "SELECT users.username FROM users where \n" +
                    "users.username IN (SELECT users.username\n" +
                    "FROM users, testResults, quiz\n" +
                    "WHERE \n" +
                    "users.user_id = testResults.userID\n" +
                    "AND topic = '"  + topic + "' \n" +
                    "AND  quiz.questionID = testResults.questionID)\n" +
                    "AND users.username NOT IN (select users.username \n" +
                    "FROM testResults , users , quiz \n" +
                    "WHERE \n" +
                    "users.user_id = testResults.userID\n" +
                    "AND quiz.questionID = testResults.questionID \n" +
                    "AND topic = '"  + topic + "' \n" +
                    "AND  quiz.answer = testResults.answer);";


            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                String username = resultSet.getString(1);

                System.out.printf("User info: \n\t username: %s \n\t total points: 0\n", username);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
