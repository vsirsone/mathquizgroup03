CREATE TABLE users (
user_id int (11) not null auto_increment,
username varchar (45) not null,
password varchar (45) not null,
fullname varchar (45) not null,
email varchar (45) not null,
PRIMARY KEY (user_id)
);

SELECT * FROM users;

INSERT INTO users (username, password, fullname, email) 
VALUES ('toby1', 'qwert', 'Toby Rich', 'toby2000@gmail.com');

INSERT INTO users (username, password, fullname, email) 
VALUES ('bob', '1234', 'Bob Rich', 'smallbob@gmail.com');

