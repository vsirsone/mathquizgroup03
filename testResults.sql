CREATE TABLE testResults (
userID int (11) not null,
questionID int (11) not null, 
answer varchar (45) not null);

SELECT * FROM testResults;

select *
from quiz, testresults
where testresults.questionID = quiz.questionID;

-- selects data from varios tables
select COUNT(*) 
from quiz, testresults
where testresults.questionID = quiz.questionID 
AND userID=2
AND topic = 'multiplication' 
AND testresults.answer=quiz.answer;

select *
from quiz, testresults
where testresults.questionID = quiz.questionID 
AND userID=2 
AND testresults.answer<>quiz.answer;

select SUM(points)
from quiz, testresults
where testresults.questionID = quiz.questionID
AND userID=2 
AND testresults.answer<>quiz.answer;

select username, question, quiz.answer AS 'Correct', testresults.answer AS 'Incorrect'
from quiz, testresults, users
where testresults.questionID = quiz.questionID 
AND users.user_id = testresults.userID
AND username = 'Bob'
AND testresults.answer<>quiz.answer;

select users.username from users where 
users.username in (select users.username
from users, testresults, quiz
where 
	users.user_id = testresults.userID
    and topic ='multiplication'
	and  quiz.questionID = testresults.questionID)
and users.username not in (select users.username 
from testresults , users , quiz 
where 
	users.user_id = testresults.userID
    and  quiz.questionID = testresults.questionID 
    and topic ='multiplication'
	and  quiz.answer = testresults.answer);


