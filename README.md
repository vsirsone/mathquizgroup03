<h2> Math Test For Kids <h2>

![](/quiz-test.png)

Project Idea
- To make a Math Quiz for kids

What the users can do?
- Login or Create an account
- Take a quiz on two different topics
- To see their own results as well other user results

<h3> Process Flow: <h3>

![](/Flowchart1.jpeg)

Database structure overview
![](/DB.png)
