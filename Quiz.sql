CREATE TABLE quiz (
questionID int (11) not null auto_increment,
topic varchar (45) not null,
question varchar (255) not null,
answer varchar (45) not null,
points int (11) not null,
PRIMARY KEY (questionID)
);

SELECT * FROM quiz;

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('plus_minus', '67-33+15 = ?', '49', 1);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('plus_minus', '125+5-100 = ?', '30', 1);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('plus_minus', 'Anna has got 7 apples. Peter has got 3 apples less than Anna, but John has got 5 apples more than Peter. Who of then has got the most apples?', 'john', 2);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('plus_minus', 'There were 60 books in the shop. On Monday 32 books were sold, but on Tuesday there were sold 9 books less than on Monday. How many books stayed in the shop', '5',3);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('plus_minus', 'Mom had 30 eur. She bought apples for 5 eur, candies for 3 eur and a dress for 18 eur. How much eur mom has got now?', '4', 2);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('multiplication', '5 X 9 = ?', '45', 1);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('multiplication', '12 X 2 = ?', '24', 1);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('multiplication', '6 X 11 = ?', '66', 1);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('multiplication', 'Richard is 8 years old, but hes dad is 4 times older. How old is the dad?', '32', 2);

INSERT INTO quiz (topic, question, answer, points) 
VALUES ('multiplication', 'Bob has got 7 crayons, but Kate has got 2 times more crayons than Bob. Tom has got 3 times more crayons than Bob and Kate. How much crayons has got Tom?  ', '63', 3);

select count(*) from quiz where topic = 'multiplication';

select sum(points) from quiz where topic = 'plus_minus';